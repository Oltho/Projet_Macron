package model;

import org.hibernate.validator.constraints.NotEmpty;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="BD_Tweet")


public class Tweet implements Serializable {

    public Tweet() {}

    public Tweet(int id, String tweet, String auteur){
        this.id = id;
        this.tweet = tweet;
        this.auteur = auteur;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    @Column(name="tweet")
    private String tweet;

    @Column(name="auteur")
    private String auteur;


    public int getId() {
        return this.id;
    }

    public void setId(int newId) {
        this.id = newId;
    }

    public String getTweet(){
        return this.tweet;
    }

    public void setTweet(String newTweet){
        this.tweet = newTweet;
    }

    public String getAuteur(){
        return this.auteur;
    }

    public void setAuteur(String newAuteur){
        this.tweet = newAuteur;
    }

}
