package com.webPortail.springboot.repositories;

import java.util.List;

import com.webPortail.springboot.model.Authentification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthentificationRepository extends JpaRepository<Authentification, Long>{
	List<Authentification> findByUsername(String username);
}
