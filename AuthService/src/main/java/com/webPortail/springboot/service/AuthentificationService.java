package com.webPortail.springboot.service;

import java.util.List;

import com.webPortail.springboot.model.Authentification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webPortail.springboot.model.Authentification;
import com.webPortail.springboot.repositories.AuthentificationRepository;

@Service("authentificationService")
@Transactional
public class AuthentificationService implements IAuthentificationService{

	@Autowired
	private AuthentificationRepository authentificationtRepository;

	public void saveAuthentification(Authentification authentification) {
		authentificationtRepository.save(authentification);
	}

	
	@Override
	public List<Authentification> search(String search) {
		return this.authentificationtRepository.findByUsername(search);
	}
}
