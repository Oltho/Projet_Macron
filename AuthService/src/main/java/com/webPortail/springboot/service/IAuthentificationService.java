package com.webPortail.springboot.service;

import com.webPortail.springboot.model.Authentification;


import java.util.List;

public interface IAuthentificationService {

	void saveAuthentification(Authentification authentification);
	List<Authentification> search(String search);

}
