<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<title>CSC - Car Sharing Club</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
body, html {
    height: 100%;
    line-height: 1.8;
}
/* Full height image header */
.bgimg-1 {
    background-position: center;
    background-size: cover;
    background-image: url("${pageContext.servletContext.contextPath}/img/paris.jpg");
    min-height: 100%;
}
.w3-bar .w3-button {
    padding: 16px;
}

    a:hover {
        text-decoration: none;
    }
</style>
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-white w3-card" id="myNavbar">
    <a href="/DevoCarWebPortail/" class="w3-bar-item w3-button w3-wide">Car Sharing Club</a>
    <!-- Right-sided navbar links -->
    <div class="w3-right w3-hide-small">
        <a href="/DevoCarWebPortail/rechercheProfil" class="w3-bar-item w3-button"><i class="fa fa-user"></i>Recherche profil</a>
      <a href="/DevoCarWebPortail/accueil" class="w3-bar-item w3-button"><i class="fa fa-user"></i>&nbsp;Profile</a>
      <a href="/DevoCarWebPortail/tarif" class="w3-bar-item w3-button"><i class="fa fa-eur"></i>&nbsp;Tarif</a>
      <a href="/DevoCarWebPortail/place" class="w3-bar-item w3-button"><i class="fa fa-product-hunt"></i>&nbsp;Parking</a>
      <a href="/DevoCarWebPortail/addcar" class="w3-bar-item w3-button"><i class="fa fa-car""></i>&nbsp;Véhicule</a>
    </div>
    <!-- Hide right-floated links on small screens and replace them with a menu icon -->

    <a href="javascript:void(0)" class="w3-bar-item w3-button w3-right w3-hide-large w3-hide-medium" onclick="w3_open()">
      <i class="fa fa-bars"></i>
    </a>
  </div>
</div>

<!-- Sidebar on small screens when clicking the menu icon -->
<nav class="w3-sidebar w3-bar-block w3-black w3-card w3-animate-left w3-hide-medium w3-hide-large" style="display:none" id="mySidebar">
  <a href="javascript:void(0)" onclick="w3_close()" class="w3-bar-item w3-button w3-large w3-padding-16"><i class="glyphicon glyphicon-ban-circle"></i>&nbsp;Fermer</a>
    <a href="/DevoCarWebPortail/profiles" onclick="w3_close()" class="w3-bar-item w3-button">Profil</a>
  <a href="/DevoCarWebPortail/tarif" onclick="w3_close()" class="w3-bar-item w3-button">&nbsp;Tarif</a>
  <a href="/DevoCarWebPortail/place" onclick="w3_close()" class="w3-bar-item w3-button">&nbsp;Parking</a>
  <a href="/DevoCarWebPortail/addcar" onclick="w3_close()" class="w3-bar-item w3-button">&nbsp;Véhicule</a>
</nav>