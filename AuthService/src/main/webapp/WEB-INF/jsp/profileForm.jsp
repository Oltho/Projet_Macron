<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="header.jsp"></jsp:include>
<br />
<br />
<h2>Bientot membre...</h2>
<h5>Vous etes a quelques parametres d'integrer le Car Sharing Club !</h5>
<br />
<h4>Il vous suffit pour cela de configurer votre profil :</h4>
<div class="panel panel-warning">
	<form:form action='/DevoCarWebPortail/profiles/add' method='post'
		modelAttribute='profiles'>

		<div class="panel-body">
			<div class="form-group">
				<h5>
					<b>Adresse e-mail *</b>
				</h5>
				<form:input type='email' path='mail'
					pattern="[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
					title="Le mail saisi ne respecte pas le format d\'une adresse e-mail, elle doit etre de type exemple@abc.xyz ."
					class='form-control' size="20" placeholder="Ex : exemple@abc.xyz"
					required="required" />
			</div>
			<div class="form-group">
				<h5>
					<b>Nom d'utilisateur *</b>
				</h5>
				<form:input type='text' path='username' class='form-control'
					pattern="[a-zA-Z0-9_-]+"
					title='Votre nom d\'utilisateur ne respecte pas le format impose. Il ne peut contenir d\'autres caracteres que des lettres 
					(minuscule ou majuscule), des chiffres, des traits d\'union ("-") et tirets du bas ("_")'
					placeholder="Ex : Member_No-1" required="required" />
			</div>
			<div class="form-group">
				<h5>
					<b>Genre *</b>
				</h5>
				<form:radiobutton path="sex" value="Homme" />
				Homme
				<form:radiobutton path="sex" value="Femme" />
				Femme
				<form:radiobutton path="sex" value="Autre" checked="checked" />
				Autre
			</div>

			<div class="form-group">
				<h5>
					<b>Prenom *</b>
				</h5>
				<form:input type='text' pattern="[A-Z][a-z����]+(-[A-Z][a-z����]+)?"
					title="Un prenom ne doit contenir que des lettres (et eventuellement un trait d'union) et commencer par une majuscule"
					path='fname' class='form-control'
					placeholder="Ex : Francois, Marie-Helene" required="required" />
			</div>

			<div class="form-group">
				<h5>
					<b>Nom de famille *</b>
				</h5>
				<form:input type='text' pattern="[A-Z]+"
					title="Un nom ne doit contenir que des lettres et etre tout en majuscule"
					path='lname' class='form-control' placeholder="Ex : JOHNSON"
					required="required" />
			</div>

			<div class="form-group">
				<h5>
					<b>Numero de telephone *</b>
				</h5>
				<form:input path='num' pattern="0[0-9]{9}"
					title="Le numero de telephone doit etre sous la forme d\'une suite de 10 chiffres commencant par 0"
					class='form-control' placeholder="Ex : 0129384756"
					required="required" />
			</div>

			<div class="form-group">
				<h5>
					<b>Date de naissance *</b>
				</h5>
				<form:input id="bday" type='date' path='bday' min="1900-01-01"
					max="1999-01-01"
					title="La date renseignee n\'est pas valide car :\n
			- Seul les personnes majeures peuvent s\'abonner a la plateforme
			\n- La date renseignee n\'est pas coherente, elle doit etre anterieure a la date du jour"
					class='form-control' required="required" />
			</div>

			<div class="form-group">
				<h5>
					<b>Numero de rue *</b>
				</h5>
				<form:input type='text' path='numaddress'
					pattern="[0-9]{1,4}(bis|ter|quater|quinquies)?"
					title="Le numero de rue ne peut etre qu\'un chiffre, ou un nombre, potentiellement suivi de bis, ter,quater ou quinquies"
					class='form-control' placeholder="Ex : 45bis" required="required" />
			</div>

			<div class="form-group">
				<h5>
					<b>Nom de rue *</b>
				</h5>
				<form:input type='text' path='nameaddress' class='form-control'
					placeholder="Ex : avenue Montaigne" required="required" />
			</div>

			<div class="form-group">
				<h5>
					<b>Code postal *</b>
				</h5>
				<form:input type='text' pattern="[0-9]{5}" path='cp'
					class='form-control' size="20"
					title="Un code postal est une suite de 5 chiffres"
					placeholder="Ex : 75006" required="required" />
			</div>

			<div class="form-group" id="fav">
				<h5>Choississez une adresse favorite</h5>
			</div>

			<div class="form-group" id="numfav">
				<h5>
					<b>Numero de rue</b>
				</h5>
				<form:input type='text' path='numaddressfav'
					pattern="[0-9]{1,4}(bis|ter|quater|quinquies)?"
					title="Le numero de rue ne peut etre qu\'un chiffre, ou un nombre, potentiellement suivi de bis, ter,quater ou quinquies"
					class='form-control' />
			</div>

			<div class="form-group" id="namefav">
				<h5>
					<b>Nom de rue</b>
				</h5>
				<form:input type='text' path='nameaddressfav' class='form-control' />
			</div>

			<div class="form-group" id="cpfav" style="">
				<h5>
					<b>Code postal</b>
				</h5>
				<form:input type='text' pattern="[0-9]{5}" path='cpfav'
					class='form-control' placeholder="Ex : 94000" />
			</div>

			<div class="form-group" id="fav2">
				<button type="button" onclick="favAddress2()">Ajouter une seconde adresse favorite</button>
			</div>

			<div class="form-group" id="numfav2" style="display: none">
				<h5>
					<b>Numero de rue</b>
				</h5>
				<form:input type='text' path='numaddressfav2' class='form-control' size="10" />
			</div>

			<div class="form-group" id="namefav2" style="display: none">
				<h5>
					<b>Nom de rue</b>
				</h5>
				<form:input type='text' path='nameaddressfav2' class='form-control' />
			</div>

			<div class="form-group" id="cpfav2" style="display:none">
				<h5>
					<b>Code postal</b>
				</h5>
				<form:input type='text' pattern="[0-9]{5}" path='cpfav2' class='form-control' 					
				title="Le numero de rue ne peut etre qu\'un chiffre, ou un nombre, potentiellement suivi de bis, ter,quater ou quinquies"
				placeholder="Ex : 94000" />
			</div>

			<div class="form-group">
				<h5>
					<b>J'aimerais que mon vehicule soit a moins de 
					<form:input type="text" path='perimetre' pattern="[1-5]" size="2" title="Entrez un chiffre entre 1 et 5" /> 
					kilometres de mon domicile</b>
				</h5>
			</div>

			<div class="form-group">
				<h5>
					<b>Accessibilite du profil (cocher cette case pour que les autres utilisateurs puissent vous trouver)</b>
				</h5>
				<form:checkbox path="profile" value="1" />
			</div>

			<div class="form-group">
				<h5>
					<b>Type d'abonnement *</b>
				</h5>
				<form:radiobutton path="abo" value="regular" />
				Regulier
				<form:radiobutton path="abo" value="ocas" />
				Occasionnel
				<form:radiobutton path="abo" value="rare" checked="checked" />
				Rare
			</div>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-warning">Mettre a jour mon profil</button>
		</div>

	</form:form>
</div>
<script>
	var now = new Date();
	var youngest = now;
	youngest.setYear(youngest.getYear() - 18);
	console.log(youngest);
	document.getElementById("bday").max = youngest.toDateInputValue();
	console.log(document.getElementById("bday").min);

	var oldest = now;
	youngest.setYear(oldest.getYear() - 100);
	console.log(oldest);
	document.getElementById("bday").min = oldest.toDateInputValue();

	function favAddress2() {
		if (document.getElementById("numfav2").style.display === "none") {
			document.getElementById("numfav2").style.display = "";
			document.getElementById("namefav2").style.display = "";
			document.getElementById("cpfav2").style.display = "";
		} else {
			document.getElementById("numfav2").style.display = "none";
			document.getElementById("namefav2").style.display = "none";
			document.getElementById("cpfav2").style.display = "none";
		}
	}
</script>
<jsp:include page="footer.jsp"></jsp:include>