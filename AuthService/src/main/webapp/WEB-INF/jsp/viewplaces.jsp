<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Liste Place Parking</title>
</head>
<body>

	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	<h1>Liste des places de parking</h1>


	<table border="1" width="90%">
		<tr>
			<th>Id</th>
			<th>Code postal</th>
			<th>Commune/Ville</th>
			<th>Numero</th>
			<th>Rue</th>
			<th>Latitude</th>
			<th>Longitude</th>
			<th>Longueur</th>
			<th>Largeur</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td><a href="">Edit</a></td>
			<td><a href="">Delete</a></td>
		</tr>
	</table>
	<br />
	<a href="addplaceform.jsp">Add New Place</a>

</body>
</html>
