<!DOCTYPE html>
<%@ page session="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="header.jsp"/>


<div class="container">
    <br/>
    <br/>
    <br/>

    <h2>Bienvenue au Car Sharing Club</h2>
    <br/>
    <h4>La confirmation de votre adresse e-mail a bien été
        enregistrée.</h4>
    <br/>
    <h4>Vous pouvez maintenant finaliser votre inscription en
        initialisalisant les paramètres de votre compte.</h4>
    <br/> <br/>
    <form action='/DevoCarWebPortail/profiles' method='get'>

        <table>
            <tr>
                <td><input type="submit" class="btn btn-primary"
                           value="Initialiser les paramètres de mon compte"/></td>
            </tr>
        </table>
    </form>
</div>

<jsp:include page="footer.jsp"/>