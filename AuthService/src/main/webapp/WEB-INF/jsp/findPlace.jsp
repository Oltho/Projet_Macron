<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<jsp:include page="header.jsp"></jsp:include>


</br></br></br>
<ul class="nav nav-tabs">
    <li role="presentation"><a href="/DevoCarWebPortail/place/viewplaces"><i class="glyphicon glyphicon-plus" ></i>&nbsp;Consulter la liste des places de parking</a></li>
    <li role="presentation" class="active"><a href="/DevoCarWebPortail/place/findPlace"><i class="glyphicon glyphicon-search" ></i>&nbsp;Rechercher une place de parking</a></li>
    <li role="presentation"><a href="/DevoCarWebPortail/place"><i class="glyphicon glyphicon-search" ></i>&nbsp;Ajouter une place de parking</a></li>
</ul>
<div style="padding: 20px 40px;">
	<h1>Chercher une place  de parking</h1>
	<form action='/DevoCarWebPortail/place/searchPlaceID' autocomplete="off"  method="post">
	<div class="panel-body">
	<div class="form-group">
			<label> Id :</label>
				<input type="number" name="id" class='form-control'
					required pattern="[0-9]{1-5}"
					title="L'id ne doit contenir que 5 chiffres." />
			</div>
				<button type="submit" class="btn btn-primary">Valider</button>
	</div>
	<b><c:out value="${danger}"></c:out></b>
	</form>
	<form action='/DevoCarWebPortail/place/searchPlaceTOWN' autocomplete="off"  method="post">
	<div class="panel-body">
	<div class="form-group">
			<label>Commune/Ville:</label>
				<input type="text" name="commune" class='form-control'
					required pattern="[a-z A-Z]*"
					title='La commune/ville ne doit pas contenir des numeros et des caracteres speciaux.' />
			</div>
				<button type="submit" class="btn btn-primary">Valider</button>
	</div>
	<b><c:out value="${danger}"></c:out></b>
	</form>
	<form action='/DevoCarWebPortail/place/searchPlaceWHERE' autocomplete="off"  method="post">
	<div class="panel-body">
	<div class="form-group">
				<label>Latitude:</label>
				<input type="text" name="latitude" class='form-control'
					required pattern = "[0-9]+.[0-9]+"
					title='La latitude doit etre ecrite au format degré décimaux [0-9]+.[0-9]+.'/>
					</div>
			<div class="form-group">
				<label>Longitude:</label>
				<input type="text" name="longitude" class='form-control'
					required pattern = "[0-9]+.[0-9]+"
					title='La longitude doit etre ecrite au format degré décimaux [0-9]+.[0-9]+.' />
			</div>	
	<button type="submit" class="btn btn-primary">Valider</button>
	</div>
	<b><c:out value="${danger}"></c:out></b>
	</form>
	</div>
</body>
</html>
