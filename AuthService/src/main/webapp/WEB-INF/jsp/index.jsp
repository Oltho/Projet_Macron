<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="header.jsp"></jsp:include>

<!-- Header with full-height image -->
<header class="bgimg-1 w3-display-container w3-grayscale-min" id="home">
	<div class="w3-display-left w3-text-white" style="padding:48px">
		<span class="w3-jumbo w3-hide-small">Explore freedom*</span><br>
		<span class="w3-xxlarge w3-hide-large w3-hide-medium">Exmplore freedom</span><br>

		<div class="btn-group" role="group" aria-label="...">
			<a href="" class="btn btn-success btn-lg">Connexion</a>
			<a href="" class="btn btn-primary btn-lg">Inscription</a>
		</div>
	</div>
</header>

<jsp:include page="footer.jsp"></jsp:include>