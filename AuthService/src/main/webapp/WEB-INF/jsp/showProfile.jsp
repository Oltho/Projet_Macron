<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="header.jsp"></jsp:include>

<br>
<br>
<br>
<h1>Details du profil</h1>
<br>

<div class="row">
    <label class="col-sm-2">Username</label>
    <div class="col-sm-10">${profiles.username}</div>
</div>

<div class="row">
    <label class="col-sm-2">Mail</label>
    <div class="col-sm-10">${profiles.mail}</div>
</div>

<div class="row">
    <label class="col-sm-2">Nom</label>
    <div class="col-sm-10">${profiles.lname} ${profiles.fname}</div>
</div>

<div class="row">
    <label class="col-sm-2">Date de naissance</label>
    <div class="col-sm-10">${profiles.bday}</div>
</div>

<div class="row">
    <label class="col-sm-2">Adresse</label>
    <div class="col-sm-10">${profiles.numaddress} ${profiles.nameaddress} ${profiles.cp}</div>
</div>

<div class="row">
    <label class="col-sm-2">Adresse Favorite 1</label>
    <div class="col-sm-10">${profiles.numaddressfav} ${profiles.nameaddressfav} ${profiles.cpfav}</div>
</div>

<div class="row">
    <label class="col-sm-2">Adresse Favorite 2</label>
    <div class="col-sm-10">${profiles.numaddressfav2} ${profiles.nameaddressfav2} ${profiles.cpfav2}</div>
</div>

<div class="row">
    <label class="col-sm-2">Numero de telephone</label>
    <div class="col-sm-10">${profiles.num}</div>
</div>

<div class="row">
    <label class="col-sm-2">Type d'abonnement</label>
    <div class="col-sm-10">${profiles.abo}</div>
</div>



<div class="row">
    <label class="col-sm-2">Type de profil</label>
    <script>
        if(${profiles.profile}=true){
            document.write("<div class=\"col-sm-10\">Publique</div>");
        }else{
            document.write("<div class=\"col-sm-10\">Privee</div>");
        }
    </script>

</div>

<div class="row">
    <label class="col-sm-2">Perimetre</label>
    <div class="col-sm-10">${profiles.perimetre}</div>
</div>









<jsp:include page="footer.jsp"></jsp:include>