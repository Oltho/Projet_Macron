<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.webPortail.springboot.model.PlaceParking"%>
<%@ page import="java.util.List"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<jsp:include page="header.jsp"></jsp:include>

</br></br></br>
<ul class="nav nav-tabs">
    <li role="presentation" class="active"><a href="/DevoCarWebPortail/place/viewplaces"><i class="glyphicon glyphicon-plus" ></i>&nbsp;Consulter la liste des places de parking</a></li>
    <li role="presentation"><a href="/DevoCarWebPortail/place/findPlace"><i class="glyphicon glyphicon-search" ></i>&nbsp;Rechercher une place de parking</a></li>
    <li role="presentation" ><a href="/DevoCarWebPortail/place"><i class="glyphicon glyphicon-search" ></i>&nbsp;Ajouter une place de parking</a></li>
</ul>
<div style="padding: 0 40px 20px;">
	<h1>
		<a href="/DevoCarWebPortail/place/findPlace" class="btn btn-default"><span
			class="glyphicon glyphicon-chevron-left"></span>&nbsp;Retour</a>
	</h1>
	<br />
	<%-- 
    <p>${placeList}</p> --%>

	<c:choose>
		<c:when test="${placeList == null}">
			<div class="alert alert-danger" role="alert">
				<span class="glyphicon glyphicon-option-horizontal"
					aria-hidden="true"></span>${erreur}
			</div>
		</c:when>
		<c:otherwise>
			<h3>
				<div class="alert alert-info alert-dismissible" role="alert">
					&nbsp;Places trouvées : &nbsp;<span class="badge">${placeList.size()}</span>
				</div>
			</h3>
		</c:otherwise>
	</c:choose>


	<table border="1" width="90%">
		<tr>
			<th>Id</th>
			<th>Code postal</th>
			<th>Commune/Ville</th>
			<th>Numero</th>
			<th>Rue</th>
			<th>Latitude</th>
			<th>Longitude</th>
			<th>Longueur</th>
			<th>Largeur</th>
		</tr>
		<c:forEach items="${placeList}" var="place">
			<tr>
				<td>${place.id}</td>
				<td>${place.codePostal}</td>
				<td>${place.commune}</td>
				<td>${place.numero}</td>
				<td>${place.rue}</td>
				<td>${place.latitude}</td>
				<td>${place.longitude}</td>
				<td>${place.longueur}</td>
				<td>${place.largeur}</td>
			<!--	<td><a href="/DevoCarWebPortail/place/delete/${place.id}">Delete</a></td> !-->
			</tr>

		</c:forEach>
	</table>
</div>
<div class="clearfix"></div>
