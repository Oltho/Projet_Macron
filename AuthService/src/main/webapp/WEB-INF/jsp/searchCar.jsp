<%--
  Created by IntelliJ IDEA.
  User: louisendelicher
  Date: 09/01/2018
  Time: 21:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"></jsp:include>
</br></br></br>

<style>
    h3 {
        margin: 10px 0;

    }
</style>

<ul class="nav nav-tabs">
    <li role="presentation"><a href="/DevoCarWebPortail/addcar"><i class="glyphicon glyphicon-plus" aria-hidden="true"></i>&nbsp;Créer</a></li>
    <li role="presentation" class="active"><a href="/DevoCarWebPortail/searchcar"><i class="glyphicon glyphicon-search" aria-hidden="true"></i>&nbsp;Rechercher</a></li>
</ul>
<div style="padding: 20px 40px;">
    <form action='/DevoCarWebPortail/car/results' autocomplete="off"  method='post' style="padding: 0;" class="panel panel-default col-md-6">
        <div class="panel-heading">
            <h3>Recherche par ID</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label >Identifiant :</label>
                <input type="number" name='id' class="form-control" placeholder="Entrez l'ID du véhicule" required>
            </div>

            <button type="submit" name="searchID" class="btn btn-warning" >Rechercher</button>
        </div>
    </form>

    <form action='/DevoCarWebPortail/car/results' autocomplete="off"  method='post' style="padding: 0;" class="panel panel-default col-md-6">
        <div class="panel-heading">
            <h3>Recherche par plaque d'immatriculation</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label >Numéro de plaque :</label>
                <input type="text" pattern="[A-NP-Z]{2}-[0-9]{3}-[A-NP-Z]{2}" name='plateNumber' placeholder="Entrez la plaque d'immatriculation (AA-000-AA)"  title="(AA-000-AA)"
                       class="form-control" required>
            </div>

            <button type="submit" name="searchPN" class="btn btn-warning" >Rechercher</button>
        </div>
    </form>

    <form action='/DevoCarWebPortail/car/results' autocomplete="off" method='post' style="padding: 0;" class="panel panel-default">
        <div class="panel-heading">
            <h3>Recherche multi-critères</h3>
        </div>
        <div class="panel-body">
            <div class="alert alert-info alert" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                Vous pouvez remplir un ou plusieurs champs, pour effectuer une recherche plus ou moins précise. Pour afficher tous les véhicules, validez sans rien saisir.
            </div>
            <div class="form-group">
                <label >Marque :</label>
                <input type="text" name='marque' class="form-control" placeholder="Entrez la marque du véhicule">
            </div>

            <div class="form-group">
                <label >Modèle :</label>
                <input type="text" name='modele' class="form-control" placeholder="Entrez le modèle du véhicule">
            </div>

            <div class="form-group">
                <label >Catégorie :</label>
                <select name='categorie' class="form-control">
                    <option disabled selected>Sélectionnez la catégorie</option>
                    <option>Berline</option>
                    <option>Break</option>
                    <option>Citadine</option>
                    <option>Coupé</option>
                    <option>Monospace</option>
                </select>
            </div>

            <div class="form-group">
                <label >Nombre de places :</label>
                <select name='nombrePlaces' class="form-control">
                    <option disabled selected>Sélectionnez le nombre de places</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                </select>
            </div>

            <div class="form-group">
                <label >Longeur :</label>
                <input type="text" pattern="\d{1}|\d{1}[\.]\d{1}|\d{1}[\.]\d{2}" name='longueur' placeholder="Entrez la longueur du véhicule (x ou x.x ou x.xx)" class="form-control">
            </div>

            <div class="form-group">
                <label >Largeur :</label>
                <input type="text" pattern="\d{1}|\d{1}[\.]\d{1}|\d{1}[\.]\d{2}" name='largeur' placeholder="Entrez la largeur du véhicule (x ou x.x ou x.xx)" class="form-control">
            </div>

            <button type="submit" name="searchMC" class="btn btn-warning" >Rechercher</button>
        </div>
    </form>
</div>
<jsp:include page="footer.jsp"></jsp:include>
