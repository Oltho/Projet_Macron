<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="header.jsp"></jsp:include>
<br />
<br />
<br />
<div class="panel-body">
	<h4>
		<i>/!\ Un autre abonn� utilise d�j� le nom d'utilisateur que vous
			avez saisi, veuillez en choisir un autre. /!\</i>
	</h4>
	<div class="panel panel-warning">

		<form:form action='/DevoCarWebPortail/validationProfile' method='post'
			modelAttribute='profiles'>

			<div class="form-group">
				<h4>
					<b>Nom d'utilisateur *</b>
				</h4>
				<form:input type='text' path='username' class='form-control'
					pattern="[a-zA-Z0-9_-]+"
					title='Votre nom d\'utilisateur ne respecte pas le format impos�. Il ne peut contenir d\'autres caract�res que des lettres
					 (minuscule ou majuscule), des chiffres, des traits d\'union ("-") et tirets du bas ("_")'
					placeholder="Ex : Member_No-1" required="required" />
			</div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-warning">Mettre � jour
					mon profil</button>
			</div>

		</form:form>
	</div>
</div>

<jsp:include page="footer.jsp"></jsp:include>